package com.JavaAdvance.OOP._04_Interface;

import java.util.Arrays;

public class MyExample {
    public static void main(String[] args) {

        startExample();

    }

    static void bubbleSort(Comparable[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    Comparable temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static void startExample() {
        Student[] students = {
                new Student("Petras", "Jonaitis", 22, 8),
                new Student("Jonas", "Petraitis", 23, 9),
                new Student("Adomas", "Jonaitis", 22, 8),
                new Student("Petras", "Petraitis", 24, 7),
                new Student("Jonas", "Petraitis", 23, 7),
                new Student("Petras", "Petraitis", 22, 5)
        };
        System.out.println(Arrays.toString(students));
        bubbleSort(students);
        System.out.println(Arrays.toString(students));
        System.out.println("-----------------------");

        Arrays.sort(students);
        for (Student str : students) {
            System.out.print("{" + str.name + ":" +
                    str.lastName + ":" + str.age + ":" + str.grade + "}, ");
        }
    }

    static class Student implements Comparable {
        String name;
        String lastName;
        int age;
        int grade;

        public Student(String name, String last, int age, int grade) {
            this.name = name;
            this.lastName = last;
            this.age = age;
            this.grade = grade;
        }

        @Override
        public int compareTo(Object o) {

            if (this.age == ((Student) o).age)
                return Integer.compare(this.grade, ((Student) o).grade);
            else if (this.age > ((Student) o).age)
                return 1;
            else
                return -1;
        }

        @Override
        public String toString() {
            return "{" + name + ":" + lastName + ":" + age + ":" + grade + "}";
        }
    }
}
