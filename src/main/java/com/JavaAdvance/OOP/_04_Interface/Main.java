package com.JavaAdvance.OOP._04_Interface;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Person1[] arr = {
                new Person1("Jonas", "Jonaitis", 25),
                new Person1("Petras", "Petraitis", 30),
                new Person1("Vardenis", "Pavardenis", 18),
                new Person1("Andrius", "Kurlinkus", 32)};

        System.out.println(Arrays.toString(arr));
        Person1.bubbleSort(arr);
        System.out.println(Arrays.toString(arr));

    }
}

class Person1 {
    String name;
    String lastName;
    int age;

    Person1(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    static void bubbleSort(Person1[] arr) {
        int counter = 0;
        boolean swapped;
        for (int i = 0; i < arr.length; i++) {
            swapped = false;
            for (int j = 1; j < arr.length - i; j++) { //  optimization 1 (arr.length - i)
                if (arr[j - 1].age > arr [j].age) {
                    // swap
                    Person1 temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    swapped = true;
                }
                counter++;
            }
            if (!swapped) break;
        }
        System.out.println("Counter: " + counter);
    }


    @Override
    public String toString() {
        return "{" + name + ":" + age + "}";
    }
}

