package com.JavaAdvance.OOP._04_Interface;

public class Main2 {
    public static void main(String[] args) {

    }
}

class Human1 implements Comparable {
    String name;
    String lastName;
    int age;

    Human1(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        //h.compareTo(h2) - kvietimas
        if (this.age > ((Human) o).age) {
            return 1;
        } else if (this.age < ((Human) o).age) {
            return -1;
        } else {
            return 0;
        }
    }
    static void bubbleSort(Human[] arr) {
        int counter = 0;
        boolean swapped;
        for (int i = 0; i < arr.length; i++) {
            swapped = false;
            for (int j = 1; j < arr.length - i; j++) { //  optimization 1 (arr.length - i)
                if (arr[j - 1].age > arr [j].age) {
                    // swap
                    Human temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    swapped = true;
                }
                counter++;
            }
            if (!swapped) break;
        }
        System.out.println("Counter: " + counter);
    }
}
