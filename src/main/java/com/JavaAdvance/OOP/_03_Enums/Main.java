package com.JavaAdvance.OOP._03_Enums;

public class Main {
    public static void main(String[] args) {

        for (Planets planets : Planets.values()) {
            System.out.println(planets);
        }

        System.out.println("Distance from Sun in Astronomical unit = "
                + Planets.MERCURY.distanceFromSunAU + "AU");
    }
}

enum Planets {

    MERCURY("Small Mercury", 0.4),
    VENUS("Red Venus", 0.7),
    EARTH("Our home", 1.0),
    MARS("Neighborhood Mars", 1.5),
    JUPITER("Big one", 5.2),
    SATURN("Lord of the ring", 9.5),
    URANUS("Cold Uranus", 19.2),
    NEPTUNE("Blue Neptune", 30.1);

    String prettyName;
    double distanceFromSunAU;

    Planets(String name, double distance) {
        this.prettyName = name;
        this.distanceFromSunAU = distance;

    }

    double distanceFromSun() {
        return distanceFromSunAU;
    }

    @Override
    public String toString() {
        return prettyName +
                ", distance from Sun AU = "
                + distanceFromSunAU;
    }
}