package com.JavaAdvance.OOP._02_Inheritance;

public class Main {
    public static void main(String[] args) {
//       Animal animalDog = new Dog("Waf waf");
//        System.out.println(animalDog.yieldVoice());
//
//        Animal animalCat = new Cat("Miau miau");
//        System.out.println(animalCat.yieldVoice());
//
//        Dog dog = new Dog("Waf waf");
//        System.out.println("Dog yield voice: " + dog.yieldVoice());
//
//        Cat cat = new Cat("Miau miau");
//        System.out.println("Cat yield voice: " + cat.yieldVoice());
//
        Animal[] animals = new Animal[2];
        animals[0] = new Dog("Waf waf");
        animals[1] = new Cat("Miau miau");

        for (Animal animal : animals
        ) {
            System.out.println(animal);
        }

    }
}

class Animal {

    private String voice;

    public Animal(String voice) {
        this.voice = voice;
    }

    public String yieldVoice() {
        return voice;
    }

    @Override
    public String toString() {
        return voice;
    }
}

class Dog extends Animal {

    public Dog(String voice) {
        super(voice);
    }

    public String yieldVoice() {
        return super.yieldVoice();
    }
}

class Cat extends Animal {

    public Cat(String voice) {
        super(voice);
    }

    public String yieldVoice() {
        return super.yieldVoice();
    }
}
