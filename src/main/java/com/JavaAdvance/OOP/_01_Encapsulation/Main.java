package com.JavaAdvance.OOP._01_Encapsulation;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog("Male", "Retriver");
        dog.setAge(1);
        System.out.println("Dog age: " + dog.getAge());
        System.out.println("Dog name: " + dog);

        Pocket pocket = new Pocket(100);
        System.out.println("Money: " + pocket.getMoney());
        pocket.setMoney(-20);
        System.out.println("Second money: " + pocket.getMoney());
        pocket.setMoney(3001);
        System.out.println("Third money: " + pocket.getMoney());

    }
}

class Dog {
    private String name;
    private int age;
    private String gender;
    private String race;
    private double weightKg;

    public Dog(String name, int age, String gender, String race, double weightKg) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.race = race;
        this.weightKg = weightKg;
    }

    // additional constructor which call main constructor
    public Dog(String gender, String race) {
        this("Rex", 5, gender, race, 15.0);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0) {
            System.out.println("Wrong age, we set age - 0");
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) {
        if (weightKg <= 0) {
            System.out.println("Not impossible");
            this.weightKg = 0.1;
        } else {
            this.weightKg = weightKg;
        }
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", race='" + race + '\'' +
                ", weightKg=" + weightKg +
                '}';
    }
}

class Pocket {
    private int money;

    public Pocket(int money) {
        this.money = money;
    }

    public int getMoney() {
        if (money <= 10 || money > 3000) {
            return 0;
        } else {
            return money;
        }
    }

    public void setMoney(int money) {
        if (money > 3000) {
            System.out.println("I don’t have enough space in my pocket for as much money!");
        } else if (money < 0) {
            System.out.println("Not impossible");
        }
        this.money = money;
    }
}
