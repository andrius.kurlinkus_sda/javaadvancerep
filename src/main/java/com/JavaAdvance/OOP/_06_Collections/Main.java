package com.JavaAdvance.OOP._06_Collections;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
// Sorting a collection
        List<Products> products = new ArrayList<>();
        products.add(new Products("Magnetola", 150));
        products.add(new Products("Videkas", 1500));
        products.add(new Products("CD Grotuvas", 200));

        System.out.println("Before: " + products);


    }
}

class Products implements Comparable<Products> {
    String name;
    int weight;

    Products(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "{" + name + ":" + weight + '}';
    }

    @Override
    public int compareTo(Products product) {
        if (this.weight == product.weight) {
            return this.name.compareToIgnoreCase(product.name);
        } else return this.weight - product.weight;
    }
}
