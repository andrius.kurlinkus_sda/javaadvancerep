package com.JavaAdvance.OOP._01_Inheritance;

public class Main {
    public static void main(String[] args) {

        Shape shape = new Square(5, 5);
        System.out.println("Length: " + shape.getLength());
        System.out.println("Area: " + shape.getArea());

    }
}

class Shape {
    private int length;
    private int width;

    public int getArea() {
        int area = length *width;
        return area;
    }

    public Shape(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}

class Rectangle extends Shape {

    public Rectangle(int length, int width) {
        super(length, width);
    }
    public int getArea() {
        return super.getArea();
    }
}

class Square extends Shape {

    public Square(int length, int width) {
        super(length, width);
    }

}